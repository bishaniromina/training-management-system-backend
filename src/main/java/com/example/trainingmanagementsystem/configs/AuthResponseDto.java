package com.example.trainingmanagementsystem.configs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class AuthResponseDto {

    private String token;
}

package com.example.trainingmanagementsystem.models.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserParamsRequest {
    private String username;
    private String pass;
    private String firstName;
    private String lastName;
    private String email;

}

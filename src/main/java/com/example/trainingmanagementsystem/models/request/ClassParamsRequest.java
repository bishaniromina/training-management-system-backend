package com.example.trainingmanagementsystem.models.request;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClassParamsRequest {
    private String name;
    private LocalDate date;
    private Long courseId;

}

package com.example.trainingmanagementsystem.models.request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleParamsRequest {
    private LocalDate date;
    private String subject;

}

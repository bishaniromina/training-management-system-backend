package com.example.trainingmanagementsystem.models.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ParticipantDto {

    private String name;
    private String lastName;
    private String username;
    private String status;
}

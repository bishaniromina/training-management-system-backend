package com.example.trainingmanagementsystem.exeption;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GenericHandlingError extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {NotFoundException.class})
    protected ResponseEntity<ErrorData> handleGlobalException(NotFoundException e) {
        return new ResponseEntity<>(new ErrorData(e.getCode(), e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}

package com.example.trainingmanagementsystem.repository;
import com.example.trainingmanagementsystem.entity.ParticipantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipantRepository  extends JpaRepository<ParticipantEntity, Long> {

     List<ParticipantEntity> findByStatus(String status);

     List<ParticipantEntity> findByCourseIdAndUserEntityId(Long courseId, Long userId );



}

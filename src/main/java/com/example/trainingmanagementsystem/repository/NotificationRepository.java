package com.example.trainingmanagementsystem.repository;

import com.example.trainingmanagementsystem.entity.NotificationEntity;
import com.example.trainingmanagementsystem.models.response.NotificationDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<NotificationEntity, Long> {

    List<NotificationEntity> findAllByOrderByDateDesc();

}

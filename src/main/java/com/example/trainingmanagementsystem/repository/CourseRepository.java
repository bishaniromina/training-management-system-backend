package com.example.trainingmanagementsystem.repository;
import com.example.trainingmanagementsystem.entity.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository

public interface CourseRepository extends JpaRepository<CourseEntity,Long>{

    List<CourseEntity> findByParticipantEntitiesUserEntityId(Long userId);
}

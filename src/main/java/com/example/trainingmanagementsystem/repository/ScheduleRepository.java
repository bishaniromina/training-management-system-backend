package com.example.trainingmanagementsystem.repository;
import com.example.trainingmanagementsystem.entity.ScheduleEntity;
import com.example.trainingmanagementsystem.models.response.ScheduleDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ScheduleRepository extends JpaRepository<ScheduleEntity, Long> {

    List<ScheduleEntity> findByClazzId(Long classId);

    List<ScheduleEntity> findByDate(LocalDate date);
}

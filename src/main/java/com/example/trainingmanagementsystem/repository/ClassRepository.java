package com.example.trainingmanagementsystem.repository;
import com.example.trainingmanagementsystem.entity.ClassEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassRepository extends JpaRepository<ClassEntity, Long> {

    List<ClassEntity>findByCourseId(Long courseId);

}

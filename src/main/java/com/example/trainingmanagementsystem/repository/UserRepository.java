package com.example.trainingmanagementsystem.repository;

import com.example.trainingmanagementsystem.entity.UserEntity;
import com.example.trainingmanagementsystem.models.CurrentUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    Optional<UserEntity> findByUsername(String username);

    @Query("select new com.example.trainingmanagementsystem.models.CurrentUser(u.id) from UserEntity u where u.username = :username")
    CurrentUser getCurrentUserByUsername(@Param("username") String username);



}

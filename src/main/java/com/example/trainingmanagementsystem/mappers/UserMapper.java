package com.example.trainingmanagementsystem.mappers;

import com.example.trainingmanagementsystem.entity.UserEntity;
import com.example.trainingmanagementsystem.models.request.UserParamsRequest;
import com.example.trainingmanagementsystem.models.response.UserDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto convertToDto(UserEntity userEntity);

    List<UserDto> convertListToDto(List<UserEntity> userEntityList);

    UserEntity convertToEntity (UserParamsRequest userParamsRequest);

}

package com.example.trainingmanagementsystem.mappers;

import com.example.trainingmanagementsystem.entity.MaterialsEntity;
import com.example.trainingmanagementsystem.models.request.MaterialsParamsRequest;
import com.example.trainingmanagementsystem.models.response.MaterialsDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MaterialsMapper {
    MaterialsDto convertToDto(MaterialsEntity materialsEntity);

    List<MaterialsDto> convertListToDto(List<MaterialsDto> materialsDtos);

    MaterialsEntity convertToEntity (MaterialsParamsRequest materialsParamsRequest);

}

package com.example.trainingmanagementsystem.mappers;

import com.example.trainingmanagementsystem.entity.CourseEntity;
import com.example.trainingmanagementsystem.models.request.CourseParamasRequest;
import com.example.trainingmanagementsystem.models.response.CourseDto;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface CourseMapper {

   CourseDto convertToDto(CourseEntity courseEntity);

    List<CourseDto> convertListToDto(List<CourseDto> courseDtoList);

    CourseEntity convertToEntity (CourseParamasRequest courseParamasRequest);

}

package com.example.trainingmanagementsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.management.relation.Role;
import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "user")
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String username;
    public String pass;
    public String firstName;
    public String lastName;
    public String email;


    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.REMOVE)
    private List<ParticipantEntity>participantEntities;

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.REMOVE)
    private List<NotificationEntity>notificationEntity;

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.REMOVE)
    private List<ScheduleEntity>scheduleEntity;

    @OneToOne
    @JoinColumn(name = "roleId")
    private RoleEntity roleEntity;
}

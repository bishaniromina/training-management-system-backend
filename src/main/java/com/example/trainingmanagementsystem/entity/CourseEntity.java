package com.example.trainingmanagementsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "course")
public class CourseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "course",cascade = CascadeType.REMOVE)
    private List<ClassEntity>classEntity;

    @OneToMany(mappedBy = "course", cascade = CascadeType.REMOVE)
    private List<ParticipantEntity>participantEntities;




}

package com.example.trainingmanagementsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "class")
public class ClassEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String name;
    public LocalDate startingDate;

    @OneToMany(mappedBy = "classEntity", cascade = CascadeType.REMOVE)
    private List<NotificationEntity>notificationEntity;

   @ManyToOne
   @JoinColumn(name = "courseId")
    private CourseEntity course;

   @OneToMany(mappedBy = "clazz",cascade = CascadeType.REMOVE)
   private List<ScheduleEntity>scheduleEntity;

    @OneToMany(mappedBy = "classEntity")
    private List<MaterialsEntity> materials;

    @OneToMany(mappedBy = "classEntity")
    private List<ParticipantEntity> participants;
}


package com.example.trainingmanagementsystem.service;

import com.example.trainingmanagementsystem.models.response.CourseDto;

import java.util.List;

public interface CourseService<T, R> extends BaseService<T,R>{
    List<CourseDto> readCoursesOfTheAssignedParticipant(Long userId);
}

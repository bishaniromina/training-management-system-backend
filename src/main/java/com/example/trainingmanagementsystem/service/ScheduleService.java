package com.example.trainingmanagementsystem.service;

import com.example.trainingmanagementsystem.models.request.ScheduleParamsRequest;
import com.example.trainingmanagementsystem.models.response.NotificationDto;
import com.example.trainingmanagementsystem.models.response.ScheduleDto;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleService<T,R> extends BaseService<T,R>{
    List<ScheduleDto> readScheduleByClassId(Long classId);

    List<ScheduleDto>readScheduleByDate(LocalDate date);

    ScheduleDto postSchedule(ScheduleParamsRequest scheduleParamsRequest, Long id, Long userId);

}

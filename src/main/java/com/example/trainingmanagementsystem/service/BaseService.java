package com.example.trainingmanagementsystem.service;

import java.util.List;

public interface BaseService<T,R> {
    R save(T t);

    R update(long id, T t);

    void delete(long id);

    List<R> read();
}

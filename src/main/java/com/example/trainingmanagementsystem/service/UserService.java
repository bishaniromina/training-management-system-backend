package com.example.trainingmanagementsystem.service;

import org.springframework.security.core.userdetails.UserDetails;

public interface UserService<T,R> extends BaseService<T,R>{

    UserDetails loadUserByUsername(String username);
}

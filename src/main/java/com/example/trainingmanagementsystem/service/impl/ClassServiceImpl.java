package com.example.trainingmanagementsystem.service.impl;

import com.example.trainingmanagementsystem.configs.CurrentUserProvider;
import com.example.trainingmanagementsystem.entity.ClassEntity;
import com.example.trainingmanagementsystem.entity.CourseEntity;
import com.example.trainingmanagementsystem.entity.ParticipantEntity;
import com.example.trainingmanagementsystem.exeption.NotFoundException;
import com.example.trainingmanagementsystem.repository.ClassRepository;
import com.example.trainingmanagementsystem.repository.CourseRepository;
import com.example.trainingmanagementsystem.repository.ParticipantRepository;
import com.example.trainingmanagementsystem.models.request.ClassParamsRequest;
import com.example.trainingmanagementsystem.models.response.ClassDto;
import com.example.trainingmanagementsystem.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
public class ClassServiceImpl implements ClassService<ClassParamsRequest, ClassDto> {

    @Autowired
    private ClassRepository classRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private CurrentUserProvider currentUserProvider;

    @Override
    public ClassDto save(ClassParamsRequest classParamsRequest) {
        CourseEntity courseEntity = courseRepository.findById(classParamsRequest.getCourseId()).orElseThrow(() -> new NotFoundException("Course id not valid", 404));

        var classEntity = ClassEntity.builder()
                .name(classParamsRequest.getName())
                .startingDate(classParamsRequest.getDate())
                .course(courseEntity)
                .build();
        classRepository.save(classEntity);
        return ClassDto.builder().id(classEntity.getId()).name(classEntity.getName()).build();
    }

    //practice
    @Override
    public ClassDto update(long id, ClassParamsRequest classParamsRequest) {
        ClassEntity classEntity = classRepository.findById(id).orElseThrow(() -> new NotFoundException("Class id not valid", 404));
        classEntity.setName(classParamsRequest.getName());
        classEntity.setStartingDate(classParamsRequest.getDate());
        classRepository.save(classEntity);
        return ClassDto.builder().name(classEntity.getName()).build();
    }

    //practice
    @Override
    public void delete(long id) {
        classRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User Not Found", 404));
        classRepository.deleteById(id);
    }

    @Override
    public List<ClassDto> read() {
        return null;
    }

    //Romina and Odeta task
    @Override
    public List<ClassDto> readClassByCourseId(Long courseId) {
        List<ClassEntity> classEntities = classRepository.findByCourseId(courseId);
        return classEntities.stream().map((classEntity ->
                ClassDto.builder()
                        .id(classEntity.getId())
                        .date(classEntity.getStartingDate())
                        .name(classEntity.getName())
                        .build()
        )).collect(Collectors.toList());

    }

    @Override
    public List<ClassDto> getParticipantClasses(Long courseId) {
        Long userId = currentUserProvider.getCurrentUser().getId();
        List<ParticipantEntity> participants = participantRepository.findByCourseIdAndUserEntityId(userId, courseId);
        return participants.stream().map((participantEntity -> {
            var course = classRepository.findById(participantEntity.getClassEntity().getId());
            return ClassDto.builder()
                    .id(course.get().getId())
                    .name(course.get().getName())
                    .date(course.get().getStartingDate())
                    .build();
        })).collect(Collectors.toList());

    }
}

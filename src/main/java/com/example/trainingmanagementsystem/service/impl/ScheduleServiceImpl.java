package com.example.trainingmanagementsystem.service.impl;

import com.example.trainingmanagementsystem.entity.ClassEntity;
import com.example.trainingmanagementsystem.entity.ScheduleEntity;
import com.example.trainingmanagementsystem.entity.UserEntity;
import com.example.trainingmanagementsystem.exeption.NotFoundException;
import com.example.trainingmanagementsystem.repository.ClassRepository;
import com.example.trainingmanagementsystem.repository.ScheduleRepository;
import com.example.trainingmanagementsystem.repository.UserRepository;
import com.example.trainingmanagementsystem.models.request.ScheduleParamsRequest;
import com.example.trainingmanagementsystem.models.response.ScheduleDto;
import com.example.trainingmanagementsystem.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ScheduleServiceImpl implements ScheduleService<ScheduleParamsRequest, ScheduleDto> {

    @Autowired
    private ScheduleRepository scheduleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ClassRepository classRepository;


    @Override
    public ScheduleDto save(ScheduleParamsRequest scheduleParamsRequest) {
        return null;
    }

    @Override
    public ScheduleDto update(long id, ScheduleParamsRequest scheduleParamsRequest) {
        return null;
    }

    //practice
    @Override
    public void delete(long id) {
        scheduleRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Schedule Not Found",404));
        scheduleRepository.deleteById(id);
    }

    @Override
    public List<ScheduleDto> read() {
        return null;
    }

    //Romina and Odeta task
    @Override
    public List<ScheduleDto> readScheduleByClassId(Long classId) {
        List<ScheduleEntity> scheduleEntities = scheduleRepository.findByClazzId(classId);
        return scheduleEntities.stream().map((scheduleEntity ->
                ScheduleDto.builder()
                        .id(scheduleEntity.getId())
                        .subject(scheduleEntity.getSubject())
                        .date(scheduleEntity.getDate())
                        .trainerName(scheduleEntity.getUserEntity().getFirstName())
                        .build()
        )).collect(Collectors.toList());

    }

    //Odeta-Task
    @Override
    public List<ScheduleDto> readScheduleByDate(LocalDate date) {
        List<ScheduleEntity> scheduleEntities = scheduleRepository.findByDate(date);
        return scheduleEntities.stream().map((scheduleEntity ->
                ScheduleDto.builder()
                        .subject(scheduleEntity.getSubject())
                        .date(scheduleEntity.getDate())
                        .trainerName(scheduleEntity.getUserEntity().getFirstName())
                        .build()
        )).collect(Collectors.toList());
    }

    //practice-Dea
    @Override
    public ScheduleDto postSchedule(ScheduleParamsRequest scheduleParamsRequest, Long id, Long userId) {
       UserEntity userEntity= userRepository.findById(userId)
               .orElseThrow(() -> new NotFoundException("User id not valid",404));
       ClassEntity classEntity= classRepository.findById(id)
               .orElseThrow(() -> new NotFoundException("Class id not valid",404));
        ScheduleEntity scheduleEntity= ScheduleEntity.builder()
                .subject(scheduleParamsRequest.getSubject())
                .date(scheduleParamsRequest.getDate())
                .userEntity(userEntity)
                .clazz(classEntity)
                .build();
        scheduleRepository.save(scheduleEntity);
        return ScheduleDto.builder().id(scheduleEntity.getId()).build();

    }


};
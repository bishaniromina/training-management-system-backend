package com.example.trainingmanagementsystem.service.impl;

import com.example.trainingmanagementsystem.entity.CourseEntity;
import com.example.trainingmanagementsystem.entity.ParticipantEntity;
import com.example.trainingmanagementsystem.entity.UserEntity;
import com.example.trainingmanagementsystem.exeption.NotFoundException;
import com.example.trainingmanagementsystem.repository.ClassRepository;
import com.example.trainingmanagementsystem.repository.CourseRepository;
import com.example.trainingmanagementsystem.repository.ParticipantRepository;
import com.example.trainingmanagementsystem.repository.UserRepository;
import com.example.trainingmanagementsystem.models.request.ParticipantParamsRequest;
import com.example.trainingmanagementsystem.models.response.ClassDto;
import com.example.trainingmanagementsystem.models.response.ParticipantDto;
import com.example.trainingmanagementsystem.models.response.UserDto;
import com.example.trainingmanagementsystem.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParticipantServiceImpl implements ParticipantService<ParticipantParamsRequest, ParticipantDto> {

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private ClassRepository classRepository;


    @Override
    public ParticipantDto save(ParticipantParamsRequest participantParamsRequest) {
        return null;
    }


    @Override
    public ParticipantDto update(long id, ParticipantParamsRequest participantParamsRequest) {

        return null;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public List<ParticipantDto> read() {
        return null;
    }

    @Override
    public void joinCourseRequest(Long userId, Long courseId) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User id not valid", 404));
        CourseEntity courseEntity = courseRepository.findById(courseId).orElseThrow(() -> new NotFoundException("User id not valid", 404));
        ParticipantEntity participantEntity = ParticipantEntity.builder()
                .status("pending")
                .userEntity(userEntity)
                .course(courseEntity)
                .build();
        participantRepository.save(participantEntity);


    }

    @Override
    public List<ParticipantDto> getPendingStatus() {
        List<ParticipantEntity> participantEntities = participantRepository.findByStatus("pending");
        return participantEntities.stream().map((participantEntity ->
                ParticipantDto.builder()
                        .username(participantEntity.getUserEntity().getUsername())
                        .name(participantEntity.getCourse().getName())
                        .status(participantEntity.getStatus())
                        .build()
        )).collect(Collectors.toList());


    }

    @Override
    public ParticipantDto updateStatus(Long id, String status) {
        ParticipantEntity participantEntity = participantRepository.findById(id).orElseThrow(() -> new NotFoundException("User id not valid", 404));
        participantEntity.setStatus(status);
        participantRepository.save(participantEntity);
        return ParticipantDto.builder().status(participantEntity.getStatus()).build();

    }

}



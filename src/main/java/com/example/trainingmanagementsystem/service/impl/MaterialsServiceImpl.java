package com.example.trainingmanagementsystem.service.impl;

import com.example.trainingmanagementsystem.entity.MaterialsEntity;
import com.example.trainingmanagementsystem.exeption.NotFoundException;
import com.example.trainingmanagementsystem.mappers.MaterialsMapper;
import com.example.trainingmanagementsystem.repository.MaterialsRepository;
import com.example.trainingmanagementsystem.models.request.MaterialsParamsRequest;
import com.example.trainingmanagementsystem.models.response.MaterialsDto;
import com.example.trainingmanagementsystem.service.MaterialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MaterialsServiceImpl implements MaterialsService<MaterialsParamsRequest, MaterialsDto> {

    @Autowired
    private MaterialsRepository materialsRepository;
    @Autowired
    private MaterialsMapper materialsMapper;

    @Override
    public MaterialsDto save(MaterialsParamsRequest materialsParamsRequest) {
        var materialsEntity = materialsMapper.convertToEntity(materialsParamsRequest);
        materialsRepository.save(materialsEntity);
        return materialsMapper.convertToDto(materialsEntity);
    }

    @Override
    public MaterialsDto update(long id, MaterialsParamsRequest materialsParamsRequest) {
        MaterialsEntity materialsEntity= materialsRepository.findById(id).orElseThrow(() -> new NotFoundException("Material id not valid",404));
        materialsEntity.setMessage(materialsParamsRequest.getMessage());
        materialsRepository.save(materialsEntity);
        return MaterialsDto.builder().message(materialsEntity.getMessage()).build();
    }

    @Override
    public void delete(long id) {
        materialsRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Materilas Not Found", 404));
        materialsRepository.deleteById(id);

    }

    @Override
    public List<MaterialsDto> read() {
        return null;
    }

    @Override
    public List<MaterialsDto> readMaterialsById(Long id) {
        Optional<MaterialsEntity> materials = materialsRepository.findById(id);
        return materials.stream().map((materialsEntity ->
                MaterialsDto.builder()
                        .id(materialsEntity.getId())
                        .message(materialsEntity.getMessage())
                        .build()
        )).collect(Collectors.toList());
    }
}

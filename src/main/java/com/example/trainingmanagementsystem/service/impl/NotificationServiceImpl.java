package com.example.trainingmanagementsystem.service.impl;

import com.example.trainingmanagementsystem.configs.CurrentUserProvider;
import com.example.trainingmanagementsystem.entity.ClassEntity;
import com.example.trainingmanagementsystem.entity.NotificationEntity;
import com.example.trainingmanagementsystem.entity.UserEntity;
import com.example.trainingmanagementsystem.exeption.NotFoundException;
import com.example.trainingmanagementsystem.repository.ClassRepository;
import com.example.trainingmanagementsystem.repository.NotificationRepository;
import com.example.trainingmanagementsystem.repository.UserRepository;
import com.example.trainingmanagementsystem.models.request.NotificationParamsRequest;
import com.example.trainingmanagementsystem.models.response.NotificationDto;
import com.example.trainingmanagementsystem.models.response.SaveNotificationDto;
import com.example.trainingmanagementsystem.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NotificationServiceImpl implements NotificationService<NotificationDto, NotificationParamsRequest> {

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ClassRepository classRepository;

    @Autowired
    private CurrentUserProvider currentUserProvider;

    @Override
    public NotificationParamsRequest save(NotificationDto notificationDto) {
        return null;
    }

    @Override
    public NotificationParamsRequest update(long id, NotificationDto notificationDto) {
        return null;
    }

    @Override
    public void delete(long id) {

    }

    @Override
    public List<NotificationParamsRequest> read() {
        return null;
    }

    //Odeta-Task
    @Override
    public SaveNotificationDto postAMessage(String message, Long classId) {
        Long userId = currentUserProvider.getCurrentUser().getId();
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User id not valid", 404));
        ClassEntity classEntity = classRepository.findById(classId).orElseThrow(() -> new NotFoundException("User id not valid", 404));
        NotificationEntity notificationEntity = NotificationEntity.builder()
                .userEntity(userEntity)
                .classEntity(classEntity)
                .message(message)
                .date(LocalDate.now())
                .build();
        notificationRepository.save(notificationEntity);
        return SaveNotificationDto.builder().notificationId(notificationEntity.getNotificationId()).build();
    }

    //Odeta-task
    @Override
    public List<NotificationDto> orderMessageByDate() {
        List<NotificationEntity> notificationEntities = notificationRepository.findAllByOrderByDateDesc();
        return notificationEntities.stream().map(notificationEntity ->
                NotificationDto.builder()
                        .date(notificationEntity.getDate())
                        .message(notificationEntity.getMessage())
                        .build()
        ).collect(Collectors.toList());
    }


}

package com.example.trainingmanagementsystem.service.impl;

import com.example.trainingmanagementsystem.entity.CourseEntity;
import com.example.trainingmanagementsystem.exeption.NotFoundException;
import com.example.trainingmanagementsystem.mappers.CourseMapper;
import com.example.trainingmanagementsystem.repository.CourseRepository;
import com.example.trainingmanagementsystem.models.request.CourseParamasRequest;
import com.example.trainingmanagementsystem.models.response.CourseDto;
import com.example.trainingmanagementsystem.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CourseServiceImp implements CourseService<CourseParamasRequest, CourseDto> {

    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private CourseMapper courseMapper;

    //practice
    @Override
    public CourseDto save(CourseParamasRequest courseParamasRequest) {
        var courseEntity = CourseEntity.builder()
                .name(courseParamasRequest.getName())
                .build();
        courseRepository.save(courseEntity);
        return CourseDto.builder().id(courseEntity.getId()).name(courseEntity.getName()).build();
    }

    //practice
    @Override
    public CourseDto update(long id, CourseParamasRequest courseParamasRequest) {
        CourseEntity courseEntity= courseRepository.findById(id).orElseThrow(() -> new NotFoundException("User id not valid",404));
        courseEntity.setName(courseParamasRequest.getName());
        courseRepository.save(courseEntity);
        return CourseDto.builder().name(courseEntity.getName()).build();
    }


    //practice
    @Override
    public void delete(long courseId) {
        var exist= courseRepository.existsById(courseId);
        if (!exist){
            throw  new NotFoundException("Course is not valid",404);
        }
        courseRepository.deleteById(courseId);
    }

    @Override
    public List<CourseDto> read() {
        List<CourseEntity> courseEntities = courseRepository.findAll();
        return courseEntities.stream().map((courseEntity ->
                CourseDto.builder()
                        .id(courseEntity.getId())
                        .name(courseEntity.getName())
                        .build()
        )).collect(Collectors.toList());

    }
    @Override
    public List<CourseDto> readCoursesOfTheAssignedParticipant(Long userId) {
        List<CourseEntity> courseEntities = courseRepository.findByParticipantEntitiesUserEntityId(userId);
        return courseEntities.stream().map((courseEntity ->
                CourseDto.builder()
                        .id(courseEntity.getId())
                        .name(courseEntity.getName())
                        .build()

        )).collect(Collectors.toList());

    }



}


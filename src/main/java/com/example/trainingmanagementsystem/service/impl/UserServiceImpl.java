package com.example.trainingmanagementsystem.service.impl;

import com.example.trainingmanagementsystem.entity.UserEntity;
import com.example.trainingmanagementsystem.exeption.NotFoundException;
import com.example.trainingmanagementsystem.repository.UserRepository;
import com.example.trainingmanagementsystem.models.request.UserParamsRequest;
import com.example.trainingmanagementsystem.models.response.UserDto;
import com.example.trainingmanagementsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService<UserParamsRequest, UserDto>, UserDetailsService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDto save(final UserParamsRequest userParamsRequest) {
        UserEntity userEntity = UserEntity.builder()
                .username(userParamsRequest.getUsername())
                .pass((userParamsRequest.getPass()))
                .firstName(userParamsRequest.getFirstName())
                .lastName(userParamsRequest.getLastName())
                .email(userParamsRequest.getEmail())
                .build();
        userRepository.save(userEntity);
        return UserDto.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .email(userEntity.getEmail())
                .build();

    }


    @Override
    public void delete(long userId) {
        userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User Not Found", 404));
        userRepository.deleteById(userId);

    }


    @Override
    public List<UserDto> read() {
        List<UserEntity> userEntities = userRepository.findAll();
        return
                userEntities.stream().map((userEntity -> {
                    return UserDto.builder()
                            .id(userEntity.getId())
                            .username(userEntity.getUsername())
                            .firstName(userEntity.getFirstName())
                            .lastName(userEntity.getLastName())
                            .email(userEntity.getEmail())
                            .build();
                })).collect(Collectors.toList());

    }

    //practice
    @Override
    public UserDto update(long id, UserParamsRequest userParamsRequest) {
        UserEntity userEntity = userRepository.findById(id).orElseThrow(() -> new NotFoundException("User id not valid", 404));
        userEntity.setFirstName(userParamsRequest.getFirstName());
        userEntity.setLastName(userParamsRequest.getLastName());
        userEntity.setUsername(userParamsRequest.getUsername());
        userEntity.setEmail(userParamsRequest.getEmail());
        userRepository.save(userEntity);
        return UserDto.builder().firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .username(userEntity.getUsername())
                .email(userEntity.getEmail())
                .build();
    }


    @Override
    public UserDetails loadUserByUsername(String username) {
        Optional<UserEntity> userEntity = userRepository.findByUsername(username);
        if (userEntity.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        return new User(
                userEntity.get().getUsername(), userEntity.get().getPass(), new ArrayList<>());
    }
}


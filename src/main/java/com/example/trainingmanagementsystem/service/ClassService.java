package com.example.trainingmanagementsystem.service;

import com.example.trainingmanagementsystem.models.response.ClassDto;


import java.util.List;

public interface ClassService<T, R> extends BaseService<T, R> {
    List<ClassDto> readClassByCourseId(Long courseId);
    List<ClassDto> getParticipantClasses(Long courseId);



}

package com.example.trainingmanagementsystem.service;

import com.example.trainingmanagementsystem.models.request.NotificationParamsRequest;
import com.example.trainingmanagementsystem.models.response.NotificationDto;
import com.example.trainingmanagementsystem.models.response.SaveNotificationDto;

import java.util.List;

public interface NotificationService<T,R >extends BaseService<T,R>{

    SaveNotificationDto postAMessage(String message, Long classId);

    List<NotificationDto>orderMessageByDate();




}

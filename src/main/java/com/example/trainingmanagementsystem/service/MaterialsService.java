package com.example.trainingmanagementsystem.service;

import com.example.trainingmanagementsystem.models.response.MaterialsDto;

import java.util.List;

public interface MaterialsService <T, R> extends BaseService<T,R>{

    List<MaterialsDto> readMaterialsById(Long id);
}

package com.example.trainingmanagementsystem.service;


import com.example.trainingmanagementsystem.models.response.ClassDto;
import com.example.trainingmanagementsystem.models.response.ParticipantDto;

import java.util.List;

public interface ParticipantService<T,R> extends BaseService<T,R> {

     void joinCourseRequest(Long userId, Long courseId);

     List<ParticipantDto> getPendingStatus ();

     ParticipantDto updateStatus(Long id, String status);



}
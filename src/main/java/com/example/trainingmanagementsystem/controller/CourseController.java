package com.example.trainingmanagementsystem.controller;

import com.example.trainingmanagementsystem.models.request.CourseParamasRequest;
import com.example.trainingmanagementsystem.models.response.ClassDto;
import com.example.trainingmanagementsystem.models.response.CourseDto;
import com.example.trainingmanagementsystem.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*")
@RestController
public class CourseController {

    @Autowired
    private CourseService courseService;


    @PostMapping(value ="/course")
    public ResponseEntity<CourseDto> createCourse(@RequestBody CourseParamasRequest courseParamasRequest) {
        CourseDto courseDto = (CourseDto) courseService.save(courseParamasRequest);
        return ResponseEntity.ok(courseDto);
    }
    @PutMapping("/course/{courseId}")
    public ResponseEntity<CourseDto>updateACourse( @RequestBody CourseParamasRequest courseParamasRequest, @PathVariable("courseId") Long id){
        CourseDto courseDto= (CourseDto) courseService.update(id, courseParamasRequest);
        return ResponseEntity.ok(courseDto);
    }
    @GetMapping("/readCourses")
    public ResponseEntity<List<CourseDto>> readAllCourses() {
        List<CourseDto> courseDto = courseService.read();
        return ResponseEntity.ok(courseDto);
    }
    @GetMapping(value="/getClassesByCourse/{courseId}")
    public ResponseEntity<List<ClassDto>> classDto(@RequestParam String name){
        List<ClassDto> classDtoList=courseService.read();
        return ResponseEntity.ok(classDtoList);
    }
    @DeleteMapping("/deleteCourse/{courseId}")
    public void deleteCourse(@PathVariable ("courseId") Long id){
        courseService.delete(id);
    }

    @GetMapping("/teacherCourses")
    public ResponseEntity<List> displayCourses(@RequestParam Long userId) {
        List listCourses = courseService.readCoursesOfTheAssignedParticipant(userId);
        return ResponseEntity.ok(listCourses);
    }


}

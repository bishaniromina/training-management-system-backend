package com.example.trainingmanagementsystem.controller;

import com.example.trainingmanagementsystem.models.request.ClassParamsRequest;
import com.example.trainingmanagementsystem.models.response.ClassDto;
import com.example.trainingmanagementsystem.service.ClassService;
import com.example.trainingmanagementsystem.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*")
@RestController
public class ClassController {

    @Autowired
    private ClassService classService;

    @Autowired
    private ParticipantService participantService;

    @PostMapping(value= "/class")
    public ResponseEntity<ClassDto> createNewClass(@RequestBody ClassParamsRequest classParamsRequest) {
        ClassDto classDto=(ClassDto) classService.save(classParamsRequest) ;
        return ResponseEntity.ok(classDto);
    }
    @PutMapping(value = "/class/{classId}")
    public ResponseEntity<ClassDto> updateClass(@PathVariable ("classId") final Long id, @RequestBody ClassParamsRequest classParamsRequest){
        ClassDto classDto=(ClassDto) classService.update(id,classParamsRequest);
        return ResponseEntity.ok(classDto);
    }

    @GetMapping("/getClassesByCourseId/{courseId}")
    public ResponseEntity<List<ClassDto>> getClassByCourseId(@PathVariable Long courseId) {
        List<ClassDto> classDtos = classService.readClassByCourseId(courseId);
        return ResponseEntity.ok(classDtos);
    }


    @DeleteMapping("/deleteClass/{classId}")
    public void deleteClass(@PathVariable ("classId") Long id){
        classService.delete(id);
    }


    @GetMapping("/getParticipantsClasses")
    public ResponseEntity<List<ClassDto> > getClassesForParticipant(@RequestParam Long courseId) {
        List<ClassDto> classDtos = classService.getParticipantClasses(courseId);
        return ResponseEntity.ok(classDtos);
    }





    }




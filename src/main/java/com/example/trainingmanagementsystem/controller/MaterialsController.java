package com.example.trainingmanagementsystem.controller;

import com.example.trainingmanagementsystem.models.request.MaterialsParamsRequest;
import com.example.trainingmanagementsystem.models.response.MaterialsDto;
import com.example.trainingmanagementsystem.service.MaterialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MaterialsController {

    @Autowired
    private MaterialsService<MaterialsParamsRequest, MaterialsDto> materialsService;


    @PostMapping("/putMaterials")
    public ResponseEntity<MaterialsDto> createMaterials(@RequestBody MaterialsParamsRequest materialsParamsRequest) {
        MaterialsDto materialsDto = materialsService.save(materialsParamsRequest);
        return ResponseEntity.ok(materialsDto);
    }

    @GetMapping("/getMaterials/{id}")
    public ResponseEntity<List<MaterialsDto>> readAllMaterials(@PathVariable Long id) {
        List<MaterialsDto> materialsDtos = materialsService.readMaterialsById(id);
        return ResponseEntity.ok(materialsDtos);
    }

    @DeleteMapping("/deleteMaterials/{id}")
    public void deleteMaterials(@PathVariable ("id") Long id){
        materialsService.delete(id);
    }


}

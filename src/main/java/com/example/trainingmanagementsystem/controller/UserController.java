package com.example.trainingmanagementsystem.controller;
import com.example.trainingmanagementsystem.models.request.UserParamsRequest;
import com.example.trainingmanagementsystem.models.response.UserDto;
import com.example.trainingmanagementsystem.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/userRegister")
    public ResponseEntity<UserDto> createNewUser (@RequestBody UserParamsRequest userParamsRequest){
        userParamsRequest.setPass(passwordEncoder.encode(userParamsRequest.getPass()));
        UserDto userDto = (UserDto) userService.save(userParamsRequest);
        return ResponseEntity.ok(userDto);
    }

    @DeleteMapping("/deleteUser/{userId}")
    public void deleteUser(@PathVariable int userId){
          userService.delete(userId);
    }

    //practice
    @PutMapping("/updateUser/{userId}")
    public ResponseEntity<UserDto> updateUser(@PathVariable int userId, @RequestBody UserParamsRequest userParamsRequest){
        UserDto userDto= (UserDto) userService.update(userId, userParamsRequest);
        return  ResponseEntity.ok(userDto);

    }




}

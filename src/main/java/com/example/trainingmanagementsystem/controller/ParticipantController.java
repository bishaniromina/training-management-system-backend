package com.example.trainingmanagementsystem.controller;

import com.example.trainingmanagementsystem.models.response.ParticipantDto;
import com.example.trainingmanagementsystem.service.ParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ParticipantController {

    @Autowired
    private ParticipantService participantService;

    @PostMapping("/joinCourseRequest/{userId}/{courseId}")
    public void postRequestForJoiningCourse(@PathVariable Long userId, @PathVariable Long courseId) {
        participantService.joinCourseRequest(userId, courseId);
    }

    @GetMapping("/getPendingStatus")
    public ResponseEntity<List<ParticipantDto>> getParticipantWithPendingStatus() {
        List<ParticipantDto> participantDtoList = participantService.getPendingStatus();
        return ResponseEntity.ok(participantDtoList);
    }

    @PutMapping("/updateStatus")
    public ResponseEntity<ParticipantDto> updateStatus(@RequestParam String status, @RequestParam Long id) {
        ParticipantDto participantDto = participantService.updateStatus(id, status);
        return ResponseEntity.ok(participantDto);

    }



}

package com.example.trainingmanagementsystem.controller;

import com.example.trainingmanagementsystem.models.request.ScheduleParamsRequest;
import com.example.trainingmanagementsystem.models.response.ScheduleDto;
import com.example.trainingmanagementsystem.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    //practice-Dea
    @GetMapping("/getScheduleForClass/{classId}")
    public ResponseEntity<List<ScheduleDto>> readScheduleForClass(@PathVariable Long classId) {
        List<ScheduleDto> scheduleDtos = scheduleService.readScheduleByClassId(classId);
        return ResponseEntity.ok(scheduleDtos);
    }

    //practice
    @DeleteMapping("deleteScheduleById/{id}")
    public void deleteScheduleById(@PathVariable Long id) {
        scheduleService.delete(id);

    }

    //Odeta-task
    @GetMapping("getScheduleByDate")
    public ResponseEntity<List<ScheduleDto>> readScheduleByDate(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        List<ScheduleDto> scheduleDtos = scheduleService.readScheduleByDate(date);
        return ResponseEntity.ok(scheduleDtos);
    }

    //Dea-practice
    @PostMapping("/postSchedule/{userId}/{id}")
    public ResponseEntity<ScheduleDto>postSchedule(@RequestBody ScheduleParamsRequest scheduleParamsRequest, @PathVariable Long userId, @PathVariable Long id){
     ScheduleDto scheduleDto= scheduleService.postSchedule(scheduleParamsRequest, userId, id);
     return ResponseEntity.ok(scheduleDto);
    }

}

package com.example.trainingmanagementsystem.controller;

import com.example.trainingmanagementsystem.models.request.NotificationParamsRequest;
import com.example.trainingmanagementsystem.models.response.NotificationDto;
import com.example.trainingmanagementsystem.models.response.SaveNotificationDto;
import com.example.trainingmanagementsystem.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class NotificationController {

    @Autowired
    public NotificationService notificationService;

    //practice-task-Odeta
    @PostMapping("/postNotification/{classId}/{userId}")
    public ResponseEntity<SaveNotificationDto> postNotification(@RequestParam String message, @PathVariable Long classId, @PathVariable Long userId) {
        SaveNotificationDto saveNotificationDto = notificationService.postAMessage(message, userId);
        return ResponseEntity.ok(saveNotificationDto);
    }

    //practice-task-Odeta
    @GetMapping("/orderNotificationByDate/{classId}")
    public ResponseEntity<List<NotificationDto>> orderNotificationByDate(@PathVariable Long classId) {
        List<NotificationDto> notificationDtoList = notificationService.orderMessageByDate();
        return ResponseEntity.ok(notificationDtoList);

    }


}
